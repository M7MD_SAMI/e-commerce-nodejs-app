import { NextFunction } from "express";

export const errorHandler400 = (msg: string, statusCode: number, data?: {} | [] ) => {
    const error: any = new Error(msg);
    error.statusCode = statusCode;
    error.data = data;
    throw error;
}

export const errorHandler500 = (error: any, next: NextFunction) => {
    if (!error.statusCode) {
        error.statusCode = 500;
    }
    next(error);
}
