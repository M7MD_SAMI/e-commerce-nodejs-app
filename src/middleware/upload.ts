import { Request } from 'express';
import multer, { FileFilterCallback, StorageEngine, diskStorage } from 'multer';

const storage = () => {
    return diskStorage({
        destination: (req, file, cb) => {
            cb(null, './src/uploads/invoices/');
        },
        filename: (req, file, cb) => {
            cb(null, new Date().getTime() + '-' + file.originalname);
        }
    });
}

const fileFilter = (req: Request, file: Express.Multer.File, cb: FileFilterCallback) => {
    if (file.mimetype === 'pdf') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

export const upload = () => {
    return multer({
        storage: storage(),
        fileFilter: fileFilter
    });        
}