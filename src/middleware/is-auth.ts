import express, { Application, Request, Response, NextFunction } from 'express';
import jwt, { sign, verify } from 'jsonwebtoken';

export const checkAuth = (req: Request, res: Response, next: NextFunction) => {
  try {
    const token: any = req.headers.authorization?.split(' ')[1];
    const decoded: any = verify(token, process.env.SECRET + '');
    // console.log(decoded);
    (req as any).user = decoded;
    // console.log((req as any).user)
    next();
  } catch (error) {
    return res.status(401).json({
      message: `Not authenticated.`
    })
  }
}