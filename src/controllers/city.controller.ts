import { Request, Response, NextFunction } from 'express';
import { createQueryBuilder, getRepository } from 'typeorm';

import { Order, OrderItem, CartItem, Cart, Category, Product, User, City, Province } from '../entity'
import { errorHandler500 } from '../middleware/error';

export const getCities = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const cities = await getRepository(City).find({ relations: ["province"] });
        return res.json(cities);
    } catch (error) {
        errorHandler500(error, next)
    }
}