import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { Order, OrderItem, CartItem, Cart, Category, Product, User, Coupon, Address } from '../entity'
import { errorHandler500 } from '../middleware/error';

export const addAddress = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const data = req.body;
        const { line1, line2, cityId, provinceId } = data as Address;
        const AddressRepo = getRepository(Address);

        const newAddress = new Address();

        newAddress.userId = user?.id;
        newAddress.line1 = line1;
        newAddress.line2 = line2;
        newAddress.cityId = cityId;
        newAddress.provinceId = provinceId;
        // console.log('Error ==>')

        const result = await AddressRepo.save(newAddress);

        return res.status(201).json({
            data: result
        });
    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getAddress = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const AddressRepo = getRepository(Address);
        const UserRepo = getRepository(User);

        const userAddresses = await AddressRepo.createQueryBuilder("address")
            .where("address.userId = :userId", { userId: user.id })
            .getMany()

        return res.status(201).json({
            data: userAddresses
        });
    } catch (error) {
        errorHandler500(error, next)
    }
}