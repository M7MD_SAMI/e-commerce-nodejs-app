import { Request, Response, NextFunction } from 'express';
import { createQueryBuilder, getRepository } from 'typeorm';

import { Order, OrderItem, CartItem, Cart, Category, Product, User } from '../entity'

export const getUsers = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const UserRepo = getRepository(User);

        // const users = await getRepository(User).find({ relations: ["cart", "addresses"] });
        const users = await UserRepo.createQueryBuilder("user")
            .leftJoinAndSelect("user.cart", "cart")
            .leftJoinAndSelect("user.addresses", "address")
            .leftJoinAndSelect("address.city", "city")
            .leftJoinAndSelect("address.province", "province")
            .getMany();

        return res.json(users);
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}

export const getUser = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { email } = req.body;
        const users = await getRepository(User).findOne({ email });

        if (!users) {
            const error:any = new Error(`User dosn't exist`);
            error.statusCode = 401;
            throw error;
        }

        return res.status(200).json(users);

    } catch (error) {
        res.status(500).json({
            error
        })
    }
}

export const signUp = async (req: Request, res: Response, next: NextFunction) => {
    try {

        const data = req.body as User;
        const email = data.email

        console.log(data)

        const userRepo = getRepository(User);
        const cartRepo = getRepository(Cart);

        const user = await userRepo.findOne({ where: { email } });

        if (user) {
            const error:any = new Error(`User already exists`);
            error.statusCode = 400;
            throw error;
        }

        const newCart = new Cart();
        await cartRepo.save(newCart);
        data.cart = newCart
        const newUser = userRepo.create(data);
        newCart.user = newUser;
        await userRepo.save(newUser);

        return res.status(201).json({
            message: 'User created!'
        });


    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}

export const login = async (req: Request, res: Response, next: NextFunction) => {
    try {

        const { password, email } = req.body as User;
        // const email = data.email;
        const userRepo = getRepository(User);

        const user = await userRepo.findOne({ select: ['password', 'id', 'email'], where: { email } });

        if (!user) {
            const error:any = new Error(`A user with this email could not be found.`);
            error.statusCode = 401;
            throw error;
        }

        if (!(await user.comparePassword(password))) {
            const error:any = new Error(`Wrong password!`);
            error.statusCode = 401;
            throw error;
        }

        return res.status(201).json({
            token: await user.token(user),
            userId: user.id
        });
    } catch (err) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        next(err);
    }
}