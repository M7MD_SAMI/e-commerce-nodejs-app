import { count } from 'console';
import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { Order, OrderItem, CartItem, Cart, Category, Product, User, Coupon, Address } from '../entity'
import { errorHandler400, errorHandler500 } from '../middleware/error';

export const getCarts = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;

        // const carts = await getRepository(Cart).find({ relations: ["user", "cartItems"] });
        const carts = await getRepository(Cart).createQueryBuilder("cart")
            .leftJoinAndSelect("cart.user", "user")
            .leftJoinAndSelect("cart.cartItems", "cartItems")
            .leftJoinAndSelect("cartItems.product", "product")
            .getMany();

        return res.status(201).json({
            data: carts
        });
    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getUserCart = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const CartRepo = getRepository(Cart);
        const CouponRepo = getRepository(Coupon);

        console.log("USER ==>", user)
        // const carts = await getRepository(Cart).find({ relations: ["user"] });
        const cart = await CartRepo.createQueryBuilder("cart")
            .leftJoinAndSelect("cart.user", "user")
            .leftJoinAndSelect("cart.coupons", "coupons")
            .leftJoinAndSelect("cart.cartItems", "cartItem")
            .leftJoinAndSelect("cartItem.product", "product")
            .where("user.id = :id", { id: user.id })
            .getOne() as Cart;

        const userCartItem = cart?.cartItems as CartItem[];
        const coupons = cart?.coupons as Coupon[];

        console.log(cart)

        if (userCartItem.length <= 0) {
            errorHandler400(`There is no products in your cart`, 400)
        }

        let subTotal = 0;
        let couponsPercent = 0;

        for (let item of userCartItem) {
            subTotal += item.quantity * item.product.price
        }

        for (const coupon of coupons) {
            const isExpired = new Date(coupon.expireDate) <= new Date()
            if (isExpired) {
                await CouponRepo.merge(coupon, { active: false })
                await CouponRepo.save(coupon)
            } else {
                couponsPercent += coupon.percent;
            }
        }

        const tax = 0;
        const shipping = cart.shipping || 0;
        const discount = (subTotal * couponsPercent) / 100;
        const total = (subTotal - discount) + tax + shipping;

        delete cart?.coupons;
        await CartRepo.merge(cart, { total, subTotal, shipping, discount })
        const result = await CartRepo.save(cart);

        return res.status(201).json({
            data: result
        });
    } catch (error) {
        errorHandler500(error, next)
    }
}

export const addShippingAddressToCart = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const { addressId } = req.body;
        const CartRepo = getRepository(Cart);
        const CouponRepo = getRepository(Coupon);
        const AddressRepo = getRepository(Address)

        const address = await await AddressRepo.createQueryBuilder("address")
            .leftJoinAndSelect("address.city", "city")
            .leftJoinAndSelect("address.province", "province")
            .where("address.userId = :userId", { userId: user.id })
            .where("address.id = :id", { id: addressId })
            .getOne() as Address

        // Check If This Address is exist in user addresses list
        if (!address) {
            errorHandler400(`This address dosn't exist in your addresses list`, 400)
        }

        const cart = await CartRepo.createQueryBuilder("cart")
            .leftJoinAndSelect("cart.user", "user")
            .leftJoinAndSelect("cart.coupons", "coupons")
            .leftJoinAndSelect("cart.cartItems", "cartItem")
            .leftJoinAndSelect("cartItem.product", "product")
            .where("user.id = :id", { id: user.id })
            .getOne() as Cart;

        const products = cart.cartItems as CartItem[];

        if (products.length <= 0) {
            errorHandler400(`Your cart is empty to apply this coupon`, 400)
        }

        const coupons = cart?.coupons as Coupon[];
        
        let subTotal = 0;
        let couponsPercent = 0;

        for (let item of products) {
            subTotal += +item.quantity * +item.product.price
        }

        for (const coupon of coupons) {
            const isExpired = new Date(coupon.expireDate) <= new Date()
            if (isExpired) {
                await CouponRepo.merge(coupon, { active: false })
                await CouponRepo.save(coupon)
            } else {
                couponsPercent += coupon.percent;
            }
        }
        
        const tax = 0;
        const shipping = address.city.shippingCost ? address.city.shippingCost : address.province.shippingCost;
        const discount = (subTotal * couponsPercent) / 100;
        const total = (subTotal - discount) + tax + shipping;

        await CartRepo.merge(cart, { addressId, shipping, total, discount });
        const result = await CartRepo.save(cart);

        return res.status(201).json({
            data: result
        });
    } catch (error) {
        errorHandler500(error, next)
    }
}