import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { Order, OrderItem, CartItem, Cart, Category, Product, User } from '../entity'
import { errorHandler400, errorHandler500 } from '../middleware/error';

export const getCategories = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const categories = await getRepository(Category).find({ relations: ["products"] });
        return res.json({
            data: categories
        });
    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getCategory = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id: number = +req.params.id
        console.log(id)
        const CategoryRepo = await getRepository(Category);
        const category = await CategoryRepo.findOne({ id }, { relations: ["products"] });

        if (!category) {
            errorHandler400(`this category dosn't exist`, 404)
        }

        return res.status(201).json({
            data: category
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const addCategories = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let data = req.body;

        if (!data.name) {
            errorHandler400(`Please enter category name!`, 400)
        }

        const newcategories = getRepository(Category).create(data);
        const results = await getRepository(Category).save(newcategories)

        return res.status(201).json({
            data: results
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const updateCategory = async (req: Request, res: Response, next: NextFunction) => {
    try {
        console.log(req.body)
        const id = req.params.id

        const CategoryRepo = getRepository(Category);

        const category = await getRepository(Category).findOne(id) as Category;

        if (!category) {
            errorHandler400(`this category dosn't exist`, 404)
        }

        getRepository(Category).merge(category, req.body)

        const results = await getRepository(Category).save(category);

        return res.status(201).json({
            data: results
        });
    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getProductInCategory = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id = +req.params.id
        console.log(id)
        // const CategoryRepo = await getRepository(Category);
        // const category = await CategoryRepo.findOne({ id });
        // const products = category?.products;

        const category = await getRepository(Category)
            .createQueryBuilder("category")
            .leftJoinAndSelect("category.products", "product")
            .where("category.id = :id", { id })
            .getOne() as Category;

        if (!category) {
            errorHandler400(`this category dosn't exist`, 404)
        }
        
        return res.status(201).json({
            data: category.products
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const deleteCategory = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id: number = +req.params.id
        const CategoryRepo = await getRepository(Category);
        const category = await CategoryRepo.delete({ id });

        if (!category) {
            errorHandler400(`this category dosn't exist`, 404)
        }
        
        return res.status(201).json({
            status: 200,
            message: `category deleted successfully`
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}


// let same_result = () => {
//     const categories = getRepository(Category).find({ relations: ["products"] });
//     const categories =  getRepository(Category)
//             .createQueryBuilder("category")
//             .leftJoinAndSelect("category.products", "product")
//             .getMany();
// }