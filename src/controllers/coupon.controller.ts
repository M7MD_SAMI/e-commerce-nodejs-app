import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { Order, OrderItem, CartItem, Cart, Category, Product, User, Coupon } from '../entity'
import { errorHandler400, errorHandler500 } from '../middleware/error';

export const addCoupon = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const { percent, expireDate, maxUser } = req.body;
        const CouponRepo = getRepository(Coupon);

        const newCoupon = new Coupon();
        newCoupon.percent = percent;
        newCoupon.expireDate = expireDate;
        newCoupon.maxUser = maxUser;

        const results = await CouponRepo.save(newCoupon);

        return res.status(201).json(results);
    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getCoupons = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const { coupon } = req.params;
        const CouponRepo = getRepository(Coupon);

        const getCoupon = await CouponRepo.find();

        return res.status(201).json(getCoupon);
    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getCoupon = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const { coupon } = req.params;
        const CouponRepo = getRepository(Coupon);

        const getCoupon = await CouponRepo.findOne({ coupon });

        if (!getCoupon) {
            errorHandler400(`Coupon dosn't exist`, 404)
        }

        return res.status(201).json(getCoupon);
    } catch (error) {
        errorHandler500(error, next)
    }
}

export const addCouponToCart = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const { coupon } = req.params;
        const CouponRepo = getRepository(Coupon);
        const UserRepo = getRepository(User);
        const CartRepo = getRepository(Cart);

        const getCoupon = await CouponRepo.findOne({ coupon }) as Coupon;

        const isExpired = new Date(getCoupon.expireDate) <= new Date();

        if (isExpired) {
            errorHandler400(`This Coupon is expired`, 400)
        }

        if (getCoupon.maxUser === 0) {
            errorHandler400(`This Coupon used already from user`, 400)
        }

        const UserCart = await CartRepo.createQueryBuilder("cart")
            .leftJoinAndSelect("cart.user", "user")
            .leftJoinAndSelect("cart.coupons", "coupon")
            .leftJoinAndSelect("cart.cartItems", "cartItem")
            .leftJoinAndSelect("cartItem.product", "product")
            .where("user.id = :id", { id: user.id })
            .getOne() as Cart;

        const products = UserCart.cartItems as CartItem[];
        // console.log(products)
        if (products.length <= 0) {
            errorHandler400(`Your cart is empty to apply this coupon`, 400)
        }

        const coupons = UserCart.coupons as Coupon[]

        const isAdded = coupons.find(cpn => cpn.coupon == coupon);

        if (isAdded) {
            errorHandler400(`This Coupon is already added in your cart`, 400)
        }

        let subTotal = 0;
        let couponsPercent = 0;

        for (let item of products) {
            subTotal += item.quantity * item.product.price
        }

        for (const coupon of coupons) {
            const isExpired = new Date(coupon.expireDate) <= new Date()
            if (isExpired) {
                await CouponRepo.merge(coupon, { active: false })
                await CouponRepo.save(coupon)
            } else {
                couponsPercent += coupon.percent;
            }
        }

        couponsPercent += +getCoupon?.percent;

        const tax = 0;
        const shipping = UserCart.shipping || 0;
        const discount = (subTotal * couponsPercent) / 100;
        const total = (subTotal - discount) + tax + shipping;

        await CartRepo.merge(UserCart, { total, coupons: [...coupons, getCoupon], subTotal, shipping, discount });

        const result = await CartRepo.save(UserCart);

        const maxUser = getCoupon.maxUser as number - 1

        CouponRepo.merge(getCoupon, { maxUser })

        return res.status(201).json({
            data: result
        });
    } catch (error) {
        errorHandler500(error, next)
    }
}