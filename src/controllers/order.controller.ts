import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { Order, OrderItem, CartItem, Cart, Category, Product, User, Coupon } from '../entity';
import { createInvoice } from '../utils/createInvoice';

import { OrderStatus } from '../interfaces/interface';
import { errorHandler400, errorHandler500 } from '../middleware/error';

export const addOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const data = req.body as Order;
        const { content } = data

        const productRepo = getRepository(Product);
        const cartRepo = getRepository(Cart);
        const userRepo = getRepository(User);
        const cartItemRepo = getRepository(CartItem);
        const OrderRepo = getRepository(Order);
        const OrderItemRepo = getRepository(OrderItem);
        const CouponRepo = getRepository(Coupon);

        const getUser = await userRepo.createQueryBuilder("user")
            .where("user.id = :id", { id: user.id })
            .getOne();

        const userCart = await cartRepo.createQueryBuilder("cart")
            .leftJoinAndSelect("cart.coupons", "coupons")
            .leftJoinAndSelect("cart.address", "address")
            .leftJoinAndSelect("address.city", "city")
            .leftJoinAndSelect("address.province", "province")
            .where("cart.id = :id", { id: getUser?.cartId })
            .getOne() as Cart;

        const userCartItem = await cartItemRepo.createQueryBuilder("cartItems")
            .leftJoinAndSelect("cartItems.product", "product")
            // .leftJoinAndSelect("cartItems.cart", "cart", "cart.id = :id", {id: getUser?.cartId})
            .where("cartItems.cartId = :cartId", { cartId: getUser?.cartId })
            .getMany();

        const coupons = userCart?.coupons as Coupon[];

        if (userCartItem.length <= 0) {
            errorHandler400(`there is not product in your cart`, 404)
        }

        // Check if there is product has not quentity
        const productHasNotQuentity = userCartItem.filter(item => item.product.quantity <= 0);

        if (productHasNotQuentity.length) {
            errorHandler400(`Please remove these products from your cart, These Produts have not enough quentity in the store`, 404, productHasNotQuentity)
        }

        console.log("Error Hear ...", userCart.addressId)
        if (!userCart.addressId) {
            errorHandler400(`Please select shipping address or add new one`, 404)
        }

        const newOrder = new Order();
        await OrderRepo.save(newOrder);

        let subTotal = 0;
        let couponsPercent = 0;

        for (const coupon of coupons) {
            const isExpired = new Date(coupon.expireDate) <= new Date()
            if (isExpired) {
                await CouponRepo.merge(coupon, { active: false })
                await CouponRepo.save(coupon)
            } else {
                couponsPercent += coupon.percent;
            }
        }

        for (const item of userCartItem) {

            subTotal += item.quantity * item.product.price;

            const newOrderItem = new OrderItem();
            newOrderItem.order = newOrder;
            newOrderItem.product = item.product;
            newOrderItem.quantity = item.quantity;

            const updateQuantity = item.product.quantity - item.quantity;

            await productRepo.merge(item.product, { quantity: updateQuantity, stock_status: updateQuantity == 0 ? false : true });
            await productRepo.save(item.product);

            await cartItemRepo.delete({ cartId: getUser?.cartId });

            await OrderItemRepo.save(newOrderItem);
            // newOrderItem.sku = 
        }

        const tax = 0;
        const shipping = userCart.shipping;
        const discount = (subTotal * couponsPercent) / 100;
        const total = (subTotal - discount) + tax + shipping;

        const invoiceNum = `${new Date().getTime()}.pdf`;

        newOrder.user = getUser as User;
        newOrder.addressId = userCart.addressId;
        newOrder.coupons = coupons;
        newOrder.total = total;
        newOrder.subTotal = subTotal;
        newOrder.shipping = shipping;
        newOrder.discount = discount;
        newOrder.content = content;
        newOrder.invoice = `${req.protocol}://${req.get('host')}/invoice/${invoiceNum}`;

        const invoice = {
            name: getUser?.userName,
            address: userCart.address,
            items: userCartItem,
            subTotal,
            total,
            discount,
            shipping
        };

        createInvoice(invoice, invoiceNum);

        userCart.total = 0;
        userCart.subTotal = 0;
        userCart.shipping = 0;
        userCart.discount = 0;
        userCart.coupons = [];
        userCart.addressId = null;

        delete userCart?.address

        await cartRepo.save(userCart);
        await OrderRepo.merge(newOrder);
        const result = await OrderRepo.save(newOrder);

        return res.status(201).json({
            data: result
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getOrder = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const data = req.body

        const productRepo = getRepository(Product);
        const cartRepo = getRepository(Cart);
        const userRepo = getRepository(User);
        const cartItemRepo = getRepository(CartItem);
        const OrderRepo = getRepository(Order);
        const OrderItemRepo = getRepository(OrderItem);

        const userOrder = await OrderRepo.createQueryBuilder("order")
            .leftJoinAndSelect("order.orderItems", "orderItems")
            .where("order.userId = :userId", { userId: user.id })
            .getMany();

        return res.status(201).json({
            data: userOrder
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const updateOrderStatus = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { user } = req as any;
        const { orderId, status } = req.query;

        const productRepo = getRepository(Product);
        const cartRepo = getRepository(Cart);
        const userRepo = getRepository(User);
        const cartItemRepo = getRepository(CartItem);
        const OrderRepo = getRepository(Order);
        const OrderItemRepo = getRepository(OrderItem);

        const userOrder = await OrderRepo.createQueryBuilder("order")
            .leftJoinAndSelect("order.orderItems", "orderItems")
            .where("order.userId = :userId", { userId: user.id })
            .andWhere("order.id = :id", { id: orderId })
            .getOne() as Order;

        if (!userOrder) {
            errorHandler400(`There is no orders to update it`, 404)
        }

        await OrderRepo.merge(userOrder, { status: status } as Order);
        const results = await OrderRepo.save(userOrder)

        return res.status(201).json({
            data: results
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}