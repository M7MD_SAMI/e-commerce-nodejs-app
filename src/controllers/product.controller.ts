import { Request, Response, NextFunction } from 'express';
import { getRepository } from 'typeorm';
import { errorHandler400, errorHandler500 } from '../middleware/error';

import { Order, OrderItem, CartItem, Cart, Category, Product, User, Coupon } from '../entity'

export const getProducts = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const products = await getRepository(Product).find({ relations: ["categories"] })
        return res.status(200).json(products);
    } catch (err) {
        errorHandler500(err, next)
    }
}

export const addProducts = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let data = req.body;

        if (!data.name) {
            errorHandler400(`Please enter product name !`, 401)
        }

        console.log(44444444)

        if (!data.description) {
            errorHandler400(`Please enter product description !`, 401)
        }

        if (!data.regular_price) {
            errorHandler400(`Please enter product regular_price !`, 401)
        }

        if (!data.categories || data.categories.length <= 0) {
            errorHandler400(`Please select at least one category !`, 401)
        }

        data.price = data.regular_price;

        if (data.sale_price) {
            data.price = data.regular_price - (data.regular_price * data.sale_price) / 100;
            data.on_sale = true
        }

        // const categories = await getRepository(Category).find({
        //     where: data.categories
        // });

        // data.categories = categories

        const newProducts = getRepository(Product).create(data);
        const results = await getRepository(Product).save(newProducts)

        return res.status(201).json(results);

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getProduct = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id: number = +req.params.id
        console.log(id)
        const productRepo = await getRepository(Product);
        const product = await productRepo.findOne({ id }, { relations: ["categories"] });

        if (product) {
            return res.status(201).json({
                data: product
            });
        }

        errorHandler400(`Product dosn't exist`, 401)

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const updateProduct = async (req: Request, res: Response, next: NextFunction) => {
    try {

        const id = +req.params.id;
        const { sale_price, regular_price, categories } = req.body;

        const productRepo = getRepository(Product);

        const product = await productRepo.findOneOrFail({ id }, { relations: ["categories"] }) as Product;

        if (!product) {
            errorHandler400(`Product dosn't exist`, 401)
        }

        if (sale_price) {
            product.price = product.regular_price - (product.regular_price * sale_price) / 100;
            product.on_sale = true;
        }

        if (regular_price) {
            product.price = regular_price - (regular_price * product.sale_price) / 100;
            product.on_sale = true;
        }

        if (categories && categories.length) {
            product.categories = [...product.categories, ...categories]
        }

        await productRepo.merge(product, req.body)

        const results = await productRepo.save(product);

        return res.status(201).json({
            data: results
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const getCategoryHasProduct = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id = +req.params.id
        console.log(id)
        const productRepo = getRepository(Product);
        const product = await productRepo.findOne({ id });
        const categories = await product?.categories

        if (product) {
            console.log(product)
            return res.status(201).json({
                data: categories
            });
        }

        errorHandler400(`Product dosn't exist`, 401)


    } catch (error) {
        errorHandler500(error, next)
    }
}

export const deleteProduct = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const id: number = +req.params.id
        console.log(id)
        const productRepo = await getRepository(Product);
        const product = await productRepo.delete({ id });

        if (product) {
            return res.status(201).json({
                status: 201,
                message: `Product deleted successfully`
            });
        }

        errorHandler400(`Product dosn't exist`, 401)

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const removeProductFromCategory = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let { id, categoryId } = req.params as any;

        const productRepo = await getRepository(Product);
        const product = await productRepo.findOneOrFail({ id }, { relations: ["categories"] });
        const categories: Category[] = product?.categories;

        if (product) {

            product.categories = categories.filter(({ id }) => id != categoryId);
            const results = await getRepository(Product).save(product);

            return res.status(201).json({
                status: 201,
                message: `Product deleted from category successfully`,
                data: results
            });
        }

        errorHandler400(`Product dosn't exist`, 404)

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const addProductToCart = async (req: Request, res: Response, next: NextFunction) => {
    try {

        const { productId } = req.body;
        const data = req.body;
        const { user } = req as any;

        const productRepo = getRepository(Product);
        const cartRepo = getRepository(Cart);
        const userRepo = getRepository(User);
        const cartItemRepo = getRepository(CartItem);
        const CouponRepo = getRepository(Coupon);

        const product = await productRepo.findOne({ id: productId }) as Product;

        if (!product) {
            errorHandler400(`Product you need to add to cart dosen't exist`, 404)
        }

        if (product?.quantity <= 0) {
            errorHandler400(`There is not enough quantity for this product`, 404)
        }

        const getUser = await userRepo.createQueryBuilder("user")
            .leftJoinAndSelect("user.cart", "cart")
            .leftJoinAndSelect("cart.coupons", "coupons")
            .leftJoinAndSelect("cart.cartItems", "cartItems")
            .leftJoinAndSelect("cartItems.product", "product")
            // .leftJoinAndSelect("cart.cartItems", "cartItems", "cartItems.productId = :productId", { productId })
            .where("user.id = :id", { id: user.id })
            .getOne();


        const userCart = getUser?.cart as Cart;
        const userCartItem = getUser?.cart?.cartItems as CartItem[];
        const coupons = userCart?.coupons as Coupon[];

        const productIsExist = userCartItem?.find(product => product.productId == productId);

        let subTotal = 0;
        let couponsPercent = 0;

        for (let item of userCartItem) {
            subTotal += item.quantity * item.product.price
        }

        for (const coupon of coupons) {
            const isExpired = new Date(coupon.expireDate) <= new Date()
            if (isExpired) {
                await CouponRepo.merge(coupon, { active: false })
                await CouponRepo.save(coupon)
            } else {
                couponsPercent += coupon.percent;
            }
        }

        if (productIsExist) {
            const productInCartItem = productIsExist;
            const quantity: number = productInCartItem.quantity + 1;

            if (product?.quantity < quantity) {
                errorHandler400(`There is not enough quantity for this product`, 404)
            }

            // let subTotal = userCart?.subTotal;
            subTotal += product.price;

            const tax = 0;
            const shipping = userCart.shipping || 0;
            const discount = (subTotal * couponsPercent) / 100;
            const total = (subTotal - discount) + tax + shipping;

            delete userCart.cartItems;

            await cartRepo.merge(userCart, { total, subTotal, shipping, discount })
            await cartRepo.save(userCart);

            await cartItemRepo.merge(productInCartItem, { quantity });
            const results = await cartItemRepo.save(productInCartItem);
            return res.status(201).json({
                data: results
            });
        }

        const newCartItem = cartItemRepo.create();
        newCartItem.product = product;
        newCartItem.cart = userCart as Cart;
        newCartItem.quantity = 1;

        subTotal += product.price;

        const tax = 0;
        const shipping = userCart.shipping || 0;
        const discount = (subTotal * couponsPercent) / 100;
        const total = (subTotal - discount) + tax + shipping;

        delete userCart.cartItems;

        await cartRepo.merge(userCart, { total, subTotal, shipping, discount })
        await cartRepo.save(userCart);

        const results = await cartItemRepo.save(newCartItem);

        return res.status(201).json({
            data: results
        });

    } catch (error) {
        errorHandler500(error, next)
    }
}

export const deleteProductFromCart = async (req: Request, res: Response, next: NextFunction) => {
    try {

        const productId = +req.params.id;
        const { user } = req as any;

        console.log(productId)

        const productRepo = getRepository(Product);
        const cartRepo = getRepository(Cart);
        const userRepo = getRepository(User);
        const cartItemRepo = getRepository(CartItem);
        const CouponRepo = getRepository(Coupon);

        const product = await productRepo.findOne({ id: productId }) as Product;

        if (!product) {
            errorHandler400(`Product you need to remove from cart dosen't exist`, 404)
        }

        const getUser = await userRepo.createQueryBuilder("user")
            .leftJoinAndSelect("user.cart", "cart")
            .leftJoinAndSelect("cart.coupons", "coupons")
            .leftJoinAndSelect("cart.cartItems", "cartItems")
            .leftJoinAndSelect("cartItems.product", "cartItem")
            // .leftJoinAndSelect("cart.cartItems", "cartItems", "cartItems.productId = :productId", { productId })
            .where("user.id = :id", { id: user.id })
            .getOne();


        const userCart = getUser?.cart as Cart;
        const userCartItem = getUser?.cart?.cartItems as CartItem[];
        const coupons = userCart?.coupons as Coupon[];

        const productIsExist = userCartItem?.find(product => product.productId == productId);

        if (!productIsExist) {
            errorHandler400(`Product you need to delete from cart dosen't exist`, 404)
        }

        let subTotal = 0;
        let couponsPercent = 0;

        for (let item of userCartItem) {
            subTotal += item.quantity * item.product.price
        }

        for (const coupon of coupons) {
            const isExpired = new Date(coupon.expireDate) <= new Date()
            if (isExpired) {
                await CouponRepo.merge(coupon, { active: false })
                await CouponRepo.save(coupon)
            } else {
                couponsPercent += coupon.percent;
            }
        }


        subTotal -= +product.price;

        const tax = 0;
        const shipping = userCart.shipping || 0;
        const discount = (subTotal * couponsPercent) / 100;
        const total = (subTotal - discount) + tax + shipping;

        delete userCart?.cartItems;

        await cartRepo.merge(userCart, { total, subTotal, shipping, discount })
        await cartRepo.save(userCart);

        const deleteItem = await cartItemRepo.delete({ productId, cartId: userCart?.id });

        if (deleteItem) {
            return res.status(201).json({
                status: 200,
                message: `Product deleted successfully from cart`
            });
        }

    } catch (error) {
        errorHandler500(error, next)
    }
}



// const product = await getRepository(Product)
//     .createQueryBuilder("product")
//     .select(["product.id", "product.name"])
//     .leftJoinAndSelect("product.categories", "category")
//     .where("product.id = :id", { id })
//     .getOne();

// const category = await getRepository(Category)
//     .createQueryBuilder("category")
//     .leftJoinAndSelect("category.products", "product")
//     .where("category.id = :id", { id })
//     .getOne();

// const users = await getRepository(User)
//     .createQueryBuilder("user")
//     .addSelect("user.phone")
//     .addSelect("cart.id")
//     .addSelect("product.id")
//     .innerJoin(Cart, "cart", "user.id = cart.userId")
//     .innerJoin(Product, "product", "cart.id = product.cartId")
//     // .where("user.email = :email", { email })
//     .getRawMany();

// const users = await getRepository(User)
//     .createQueryBuilder("user")
//     .leftJoinAndSelect("user.products", "product")
//     .leftJoinAndSelect("user.cart", "cart")
//     .leftJoinAndSelect("cart.products", "products")
//     .where("user.email = :email", { email })
//     .getOne();