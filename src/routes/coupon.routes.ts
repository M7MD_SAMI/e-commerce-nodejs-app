import { Router } from 'express';
import { addCoupon, addCouponToCart, getCoupon, getCoupons } from '../controllers/coupon.controller';
import { checkAuth } from '../middleware/is-auth';
const router = Router();

router.post('/', checkAuth, addCoupon)
router.get('/', checkAuth, getCoupons)
router.get('/:coupon', checkAuth, getCoupon)
router.get('/:coupon/add-to-cart', checkAuth, addCouponToCart)

export default router