import { Router } from 'express';
import { getProducts, addProducts, updateProduct, getProduct, getCategoryHasProduct, deleteProduct, removeProductFromCategory, addProductToCart, deleteProductFromCart } from '../controllers/product.controller';
import { checkAuth } from '../middleware/is-auth'
const router = Router();

router.get('/', getProducts); // Get All Products
router.get('/:id', getProduct); // Get Product By Id
router.post('/', checkAuth, addProducts); // Add New Product
router.put('/:id', checkAuth, updateProduct); // Update Product By Id
router.delete('/:id', checkAuth, deleteProduct); // Delete Product By ID

/**** Product & Category ****/
router.get('/:id/categories', getCategoryHasProduct); // Get Gategories Has Product
router.delete('/:id/categories/:categoryId', checkAuth, removeProductFromCategory); // Remove Product From Category

/**** Product & Cart ****/
router.post('/add-to-cart', checkAuth, addProductToCart); // Add Product To Cart
router.delete('/:id/delete-from-cart', checkAuth, deleteProductFromCart); // Delete Product From Cart


export default router