import { Router } from 'express';
import { addAddress, getAddress } from '../controllers/address.controller';
import { checkAuth } from '../middleware/is-auth';
const router = Router();

router.post('/', checkAuth, addAddress)
router.get('/', checkAuth, getAddress)

export default router