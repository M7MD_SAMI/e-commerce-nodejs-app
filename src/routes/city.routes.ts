import { Router } from 'express';
import { getCities } from '../controllers/city.controller';
import { checkAuth } from '../middleware/is-auth';
const router = Router();

router.get('/', getCities)

export default router