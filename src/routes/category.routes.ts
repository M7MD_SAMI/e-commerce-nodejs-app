import { Router } from 'express';
const router = Router();

import { getCategories, addCategories, updateCategory, getProductInCategory, getCategory, deleteCategory } from '../controllers/category.controller';
import { checkAuth } from '../middleware/is-auth';

router.get('/', getCategories);
router.get('/:id', getCategory);
router.get('/:id/products', getProductInCategory);
router.post('/', checkAuth, addCategories);
router.put('/:id', checkAuth, updateCategory);
router.delete('/:id', checkAuth, deleteCategory);

// router.get('/categories/:id', );
// router.post('/categories', );

export default router