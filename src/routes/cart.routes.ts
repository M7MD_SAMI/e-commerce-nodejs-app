import { Router } from 'express';
import { addShippingAddressToCart, getCarts, getUserCart } from '../controllers/cart.controller';
import { checkAuth } from '../middleware/is-auth';
const router = Router();

router.get('/', checkAuth, getCarts)
router.post('/shipping-address', checkAuth, addShippingAddressToCart)
router.get('/user-cart', checkAuth, getUserCart)

export default router