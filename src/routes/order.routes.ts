import { Router } from 'express';
import { addOrder, getOrder, updateOrderStatus } from '../controllers/order.controller';
import { checkAuth } from '../middleware/is-auth';
const router = Router();

router.post('/', checkAuth, addOrder)
router.get('/', checkAuth, getOrder)
router.put('/', checkAuth, updateOrderStatus)

export default router