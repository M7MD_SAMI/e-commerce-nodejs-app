import { Router } from 'express';
import { getUser, getUsers, login, signUp } from '../controllers/user.controller';
import { checkAuth } from '../middleware/is-auth';
const router = Router();

router.get('/api/users', checkAuth, getUsers)
router.post('/api/users', checkAuth, getUser)
router.post('/auth/signup', signUp);
router.post('/auth/login', login);

export default router