const swagger = {
    swagger: "2.0",
    info: {
        description: "This is a Swagger Documentation for NodeJS API project",
        version: "1.0.0",
        title: "E-commerce API",
        contact: {
            email: "samy.ashoor@gmail.com"
        },
        license: {
            name: "Apache 2.0",
            url: "http://www.apache.org/licenses/LICENSE-2.0.html"
        }
    },
    schemes: ["http"],
    host: "localhost:4000",
    basePath: "/api",
    paths: {
        '/products': {
            get: {
                tags: [
                    "Products"
                ],
                summary: "Get All Products",
                parameters: [],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            },
            post: {
                tags: [
                    "Products"
                ],
                summary: "Add New Product",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    },
                    {
                        name: 'name',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'Product Name'
                    },
                    {
                        name: 'description',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'Product description'
                    },
                    {
                        name: 'regular_price',
                        in: 'query',
                        schema: {
                            type: 'number'
                        },
                        required: true,
                        description: 'Product regular price'
                    },
                    {
                        name: 'on_sale',
                        in: 'query',
                        schema: {
                            type: 'boolean',
                            default: 0
                        },
                        required: false,
                        description: 'If Product is Onsale'
                    },
                    {
                        name: 'quantity',
                        in: 'query',
                        schema: {
                            type: 'number'
                        },
                        required: true,
                        description: 'Product quantity'
                    },
                    {
                        name: 'categories',
                        in: 'query',
                        schema: {
                            type: 'array'
                        },
                        required: true,
                        description: 'Add product in categories'
                    },
                    {
                        name: 'orderBy',
                        in: 'query',
                        schema: {
                            type: 'string',
                            enum: ['asc', 'desc'],
                            default: 'asc'
                        },
                        required: false
                    }
                ],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            }
        },
        '/products/:id': {
            get: {
                tags: [
                    "Products"
                ],
                summary: "Get Product",
                parameters: [],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            },
            put: {
                tags: [
                    "Products"
                ],
                summary: "Update Product By Id",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            },
            delete: {
                tags: [
                    "Products"
                ],
                summary: " Delete Product By ID",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            }
        },
        '/products/:id/categories': {
            get: {
                tags: [
                    "Products"
                ],
                summary: "Get Gategories Has Product",
                parameters: [],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            }
        },
        '/products/:id/categories/:categoryId': {
            delete: {
                tags: [
                    "Products"
                ],
                summary: "Remove Product From Category",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            }
        },
        '/products/add-to-cart': {
            post: {
                tags: [
                    "Products"
                ],
                summary: "Add Product To Cart",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    },
                    {
                        name: 'productId',
                        in: 'query',
                        schema: {
                            type: 'number'
                        },
                        required: true,
                        description: 'Product Id'
                    }
                ],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            }
        },
        '/products/:id/delete-from-cart': {
            delete: {
                tags: [
                    "Products"
                ],
                summary: "Remove Product From Cart",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/Users"
                        }
                    }
                }
            }
        },
        "/users": {
            get: {
                tags: [
                    "Users"
                ],
                summary: "Get all users in system",
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: "#/definitions/Users"
                        }
                    }
                }
            }
        },
        "/auth/signup": {
            post: {
                tags: [
                    "Users"
                ],
                summary: "Sign up to system",
                parameters: [
                    {
                        name: 'username',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'username'
                    },
                    {
                        name: 'email',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: ''
                    },
                    {
                        name: 'phone',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: ''
                    },
                    {
                        name: 'password',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: ''
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: "#/definitions/Users"
                        }
                    }
                }
            }
        },
        "/auth/login": {
            post: {
                tags: [
                    "Users"
                ],
                summary: "Login to system",
                parameters: [
                    {
                        name: 'email',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: ''
                    },
                    {
                        name: 'password',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: ''
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: "#/definitions/Users"
                        }
                    }
                }
            }
        },
        "/categories": {
            get: {
                tags: [
                    "Categories"
                ],
                summary: "Get all categories in system",
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            },
            post: {
                tags: [
                    "Categories"
                ],
                summary: "Get all categories in system",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    },
                    {
                        name: 'name',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'category Name'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/categories/:id": {
            get: {
                tags: [
                    "Categories"
                ],
                summary: "Get category",
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            },
            put: {
                tags: [
                    "Categories"
                ],
                summary: "Update category",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    },
                    {
                        name: 'name',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'category Name'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            },
            delete: {
                tags: [
                    "Categories"
                ],
                summary: "delete category",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/categories/:id/products": {
            get: {
                tags: [
                    "Categories"
                ],
                summary: "Get Products in category",
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/addresses": {
            get: {
                tags: [
                    "Addresses"
                ],
                summary: "Get user addresses",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            },
            post: {
                tags: [
                    "Addresses"
                ],
                summary: "Add new address",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    },
                    {
                        name: 'userId',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'user Id'
                    },
                    {
                        name: 'line1',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'line 1'
                    },
                    {
                        name: 'line2',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: false,
                        description: 'line 2'
                    },
                    {
                        name: 'provinceId',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'province Id'
                    },
                    {
                        name: 'cityId',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'city Id'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/carts/user-cart": {
            get: {
                tags: [
                    "Carts"
                ],
                summary: "Get user cart",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/carts/shipping-address": {
            post: {
                tags: [
                    "Carts"
                ],
                summary: "Add new address",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    },
                    {
                        name: 'addressId',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: ''
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/cities": {
            get: {
                tags: [
                    "Cities"
                ],
                summary: "Get cities",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/coupon": {
            get: {
                tags: [
                    "Coupon"
                ],
                summary: "Get coupon",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            },
            post: {
                tags: [
                    "Coupon"
                ],
                summary: "Add new Coupon",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    },
                    {
                        name: 'percent',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'Add discount number as a percent'
                    },
                    {
                        name: 'expireDate',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: true,
                        description: 'Add expireDate'
                    },
                    {
                        name: 'maxUser',
                        in: 'query',
                        schema: {
                            type: 'string'
                        },
                        required: false,
                        description: 'Add max User will use this coupon'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/coupon/:id": {
            get: {
                tags: [
                    "Coupon"
                ],
                summary: "Get coupon",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/coupon/:id/add-to-cart": {
            get: {
                tags: [
                    "Coupon"
                ],
                summary: "apply coupon to cart",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        },
        "/orders": {
            get: {
                tags: [
                    "Orders"
                ],
                summary: "Get user Orders",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            },
            post: {
                tags: [
                    "Orders"
                ],
                summary: "Add new Order",
                parameters: [
                    {
                        name: 'Authorization',
                        in: 'header',
                        schema: {
                            $ref: '#/components/schemas/companyId'
                        },
                        required: true,
                        description: 'Send Bearer Token you will get it after login'
                    }
                ],
                responses: {
                    200: {
                        description: "OK",
                        schema: {
                            $ref: ""
                        }
                    }
                }
            }
        }
    },
    // definitions: {
    //     User: {
    //         required: [
    //             "name",
    //             "_id",
    //             "companies"
    //         ],
    //         properties: {
    //             _id: {
    //                 type: "integer",
    //                 uniqueItems: true
    //             },
    //             isPublic: {
    //                 type: "boolean"
    //             },
    //             name: {
    //                 type: "string"
    //             },
    //             books: {
    //                 type: "array",
    //                 items: {
    //                     type: "object",
    //                     properties: {
    //                         name: {
    //                             type: "string"
    //                         },
    //                         amount: {
    //                             type: "number"
    //                         }
    //                     }
    //                 }
    //             },
    //             companies: {
    //                 type: "array",
    //                 items: {
    //                     type: "string"
    //                 }
    //             }
    //         }
    //     },
    //     Users: {
    //         type: "array",
    //         $ref: "#/definitions/User"
    //     }
    // }
}

export default swagger;