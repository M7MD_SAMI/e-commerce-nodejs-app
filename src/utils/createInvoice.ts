import fs from 'fs';
import PDFDocument from 'pdfkit';
import path from 'path';

const generateHeader = (doc: PDFKit.PDFDocument) => {
  
  doc
    .image(getRootPath(`../../static/img/logo.png`), 50, 45, { width: 50 })
    .fillColor("#444444")
    .fontSize(20)
    .text("Name.", 110, 57)
    .fontSize(10)
    .text("Name.", 200, 50, { align: "right" })
    .text("123 Main Street", 200, 65, { align: "right" })
    .text("New York, NY, 10025", 200, 80, { align: "right" })
    .moveDown();
}

const generateCustomerInformation = (doc: PDFKit.PDFDocument, invoice: any) => {
  doc
    .fillColor("#444444")
    .fontSize(20)
    .text("Invoice", 50, 160);

  generateHr(doc, 185);

  const customerInformationTop = 200;
  console.log(invoice.address.line1)

  doc
    .fontSize(10)
    .text("Invoice Number:", 50, customerInformationTop)
    .font("Helvetica-Bold")
    .text(invoice.invoice_nr, 150, customerInformationTop)
    .font("Helvetica")
    .text("Invoice Date:", 50, customerInformationTop + 15)
    .text(formatDate(new Date()), 150, customerInformationTop + 15)
    .text("Balance Due:", 50, customerInformationTop + 30)
    .text(
      formatCurrency(invoice.subTotal - invoice.paid),
      150,
      customerInformationTop + 30
    )

    .font(getRootPath("../../static/font/coranica_allerseelen.ttf"))
    .text(rightToLeftText(invoice.name), 300, customerInformationTop)
    .font(getRootPath("../../static/font/coranica_allerseelen.ttf"))
    .text(rightToLeftText(invoice.address.line1), 300, customerInformationTop + 15)
    .text(rightToLeftText(invoice.address.line2), 300, customerInformationTop + 30)
    .text(
      invoice.address.city.nameAR +
        ", " +
        invoice.address.province.nameAR,
      300,
      customerInformationTop + 45
    )
    .moveDown();

  generateHr(doc, 267);
}

const generateInvoiceTable = (doc: PDFKit.PDFDocument, invoice: any) => {
  let i;
  const invoiceTableTop = 330;

  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    invoiceTableTop,
    "Item",
    "Description",
    "Quantity",
    "Price"
  );
  generateHr(doc, invoiceTableTop + 20);
  doc.font("Helvetica");

  for (i = 0; i < invoice.items.length; i++) {
    const item = invoice.items[i];
    const position = invoiceTableTop + (i + 1) * 30;
    generateTableRow(
      doc,
      position,
      item.product.name,
      item.product.description,
      item.quantity,
      item.product.price
      // formatCurrency(item.product.price)
    );

    generateHr(doc, position + 20);
  }

  const subtotalPosition = invoiceTableTop + (i + 1) * 30;
  generateTableRow(
    doc,
    subtotalPosition,
    "",
    "",
    "Subtotal",
    invoice.subTotal
    // formatCurrency(invoice.subTotal)
  );

  const paidToDatePosition = subtotalPosition + 20;
  generateTableRow(
    doc,
    paidToDatePosition,
    "",
    "",
    "Discount",
    invoice.discount
    // formatCurrency(invoice.paid)
  );

  const shippingPosition = paidToDatePosition + 20;
  generateTableRow(
    doc,
    shippingPosition,
    "",
    "",
    "Shipping",
    invoice.shipping
    // formatCurrency(invoice.paid)
  );

  const totalPosition = shippingPosition + 25;
  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    totalPosition,
    "",
    "",
    "Total",
    invoice.total
  );
  doc.font("Helvetica");
}

const generateFooter = (doc:PDFKit.PDFDocument) => {
  doc
    .fontSize(10)
    .text(
      "Payment is due within 15 days. Thank you for your business.",
      50,
      780,
      { align: "center", width: 500 }
    );
}

const generateTableRow = (
  doc: PDFKit.PDFDocument,
  y: number,
  item: string,
  description: string,
  quantity: string,
  lineTotal: string
) => {
  doc
    .fontSize(10)
    .text(item, 50, y)
    .text(description, 150, y)
    .text(quantity, 370, y, { width: 90, align: "right" })
    .text(lineTotal, 0, y, { align: "right" });
}

const generateHr = (doc:any, y:any) => {
  doc
    .strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(550, y)
    .stroke();
}

const formatCurrency = (cents:number) => {
  return "$" + (cents / 100).toFixed(2);
}

const formatDate = (date:Date) => {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return year + "/" + month + "/" + day;
}

const isArabic = (text:string) => {
  return text.search(/[\u0600-\u06FF]/) >= 0;
};

const rightToLeftText = (text:string) => {
  if (isArabic(text)) {
    return text.split(' ').reverse().join(' ');
  } else {
    return text;
  }
};

const getRootPath = (pathName:string) => {
  return path.join(__dirname, pathName)
}

export const createInvoice = (invoice:object, path:string) => {
  let doc = new PDFDocument({ size: "A4", margin: 50 });

  generateHeader(doc)
  generateCustomerInformation(doc, invoice);
  generateInvoiceTable(doc, invoice);
  generateFooter(doc);

  doc.end();

  const dirInvoice = './invoice';

  if (!fs.existsSync(dirInvoice)){
      fs.mkdirSync(dirInvoice);
  }
  doc.pipe(fs.createWriteStream(`${dirInvoice}/${path}`));
}
