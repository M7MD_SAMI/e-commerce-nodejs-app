import {
    BeforeInsert,
    Column,
    CreateDateColumn,
    Entity,
    Index,
    JoinColumn,
    JoinTable,
    ManyToMany,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';

import { Product, Order, Cart } from '.'

@Entity('coupon')
// @Entity()
export class Coupon {

    @PrimaryGeneratedColumn()
    id: string;

    @Column({ type: 'tinyint' })
    percent: number

    @Column({ type: 'smallint', nullable: true, default: null })
    maxUser: number | null

    @Column('varchar', { name: 'coupon', length: 15, unique: true })
    coupon: string;

    @BeforeInsert()
    async couponGenerator() {
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        this.coupon = '';
        for (var i = 0; i < 8; i++) {
            this.coupon += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        this.active = true;
    }

    @Column({ type: 'varchar', length: 50 })
    expireDate: string;

    @Column({ default: 0 })
    active: boolean;

    @ManyToMany(() => Cart, cart => cart.coupons)
    @JoinTable()
    carts: Cart[]

    @ManyToMany(() => Order, order => order.coupons, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinTable()
    orders: Order[];

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

}