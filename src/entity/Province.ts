import {Entity, PrimaryGeneratedColumn, Column, JoinTable, ManyToMany, OneToMany, OneToOne} from "typeorm";
import { City, Address } from '.';

@Entity()
export class Province {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { length: 50 })
    nameEN: string;

    @Column('varchar', { length: 50 })
    nameAR: string;

    @Column({ type: 'smallint' })
    shippingCost: number

    @OneToMany(() => City, city => city.province, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    cities: City[];

    @OneToMany(() => Address, address => address.province)
    addresses: Address[];

}