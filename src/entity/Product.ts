import { Entity, Column, PrimaryGeneratedColumn, Generated, PrimaryColumn, UpdateDateColumn, CreateDateColumn, ManyToMany, JoinTable, ManyToOne, OneToMany, JoinColumn } from "typeorm";

import { Order, OrderItem, CartItem, User, Cart, Category } from '.'

@Entity()
export class Product {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    description: string;

    @Column('float', { precision: 12 })
    price: number;

    @Column()
    regular_price: number;

    @Column({ default: 0 })
    sale_price: number;

    @Column({ default: 1 })
    stock_status: boolean;

    @Column({ default: 0 })
    on_sale: boolean;

    @Column('smallint', { name: 'quantity', default: () => "'0'" })
    quantity: number;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

    @ManyToMany(() => Category, category => category.products)
    categories: Category[]
    // categories: Promise<Category[]>

    @OneToMany(() => CartItem, cartItem => cartItem.cart)
    cartItems: CartItem[];

    @OneToMany( () => OrderItem, orderItem => orderItem.product )
	orderItems: OrderItem[];

    @ManyToOne(() => User, user => user.products)
    user: User;

    // images
}