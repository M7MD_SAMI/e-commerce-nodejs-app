import {Entity, PrimaryGeneratedColumn, Column, JoinTable, ManyToMany, ManyToOne, JoinColumn, OneToOne, OneToMany} from "typeorm";
import { Province, Address } from '.'

@Entity()
export class City {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { length: 50 })
    nameEN: string;

    @Column('varchar', { length: 50 })
    nameAR: string;

    @Column({ type: 'smallint', nullable: true, default: null })
    shippingCost: number | null

    @Column({ type: 'smallint', name: 'provinceId' })
    provinceId: number;
    
    @ManyToOne(() => Province, province => province.cities)
    @JoinColumn([{ name: 'provinceId', referencedColumnName: 'id' }])
    province: Province;
    
    @OneToMany(() => Address, address => address.city)
    addresses: Address[];

}