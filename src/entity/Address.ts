import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Province, City, User, Cart, Order } from '.'

@Entity()
export class Address {
    @PrimaryGeneratedColumn({type: 'smallint'})
    id: string;

    @Column('bigint', { name: 'userId', nullable: true })
    userId: string | null;

    @Column('varchar', { name: 'line1', nullable: false, length: 50 })
    line1: string;

    @Column('varchar', { name: 'line2', nullable: true, length: 50 })
    line2: string | null;

    @Column('varchar', { length: 50, nullable: true })
    phone: string;

    @Column({ name: 'cityId', nullable: false })
    cityId: number;
    
    @Column({ name: 'provinceId', nullable: false })
    provinceId: number;

    @Column({ default: 0 })
    primary: boolean;
    
    @ManyToOne(() => User, user => user.addresses)
    @JoinColumn([{ name: 'userId', referencedColumnName: 'id' }])
    user: User;

    @ManyToOne(() => City, city => city.addresses, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'cityId', referencedColumnName: 'id' }])
    city: City;

    @ManyToOne(() => Province, province => province.addresses, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'provinceId', referencedColumnName: 'id' }])
    province: Province;

    @OneToOne(() => Cart, cart => cart.address)
    cart: Cart;

    @OneToMany(() => Order, order => order.address)
    orders?: Order[];

}

export default Address;