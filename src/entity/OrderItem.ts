import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';

import { Product, Order } from '.'

@Index('idx_order_item_product', ['productId'], {})
@Index('idx_order_item_order', ['orderId'], {})
@Entity( 'order_item', { schema: 'shop' } )
export class OrderItem {

    @PrimaryGeneratedColumn()
    id: string;

    @Column({ name: 'productId' })
    productId: string;

    @Column('bigint', { name: 'orderId' })
    orderId: string;

    @Column('varchar', { name: 'sku', length: 100 })
    sku: string;

    @Column('smallint', { name: 'quantity', default: () => "'0'" })
    quantity: number;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

    @ManyToOne(() => Order, (order) => order.orderItems, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'orderId', referencedColumnName: 'id' }])
    order: Order;

    @ManyToOne(() => Product, (product) => product.orderItems, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'productId', referencedColumnName: 'id' }])
    product: Product; 

}