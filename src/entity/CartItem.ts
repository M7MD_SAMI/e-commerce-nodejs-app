import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    JoinColumn,
    ManyToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';

import { Cart, Product } from '.'

// @Index('idx_cart_item_product', ['productId'], {})
// @Index('idx_cart_item_cart', ['cartId'], {})
@Entity()
export class CartItem {

    @PrimaryGeneratedColumn()
    id: string;

    @Column({ name: 'productId' })
    productId: number;

    @Column({ name: 'cartId' })
    cartId: number;

    @Column('smallint', { name: 'quantity', default: () => "'0'" })
    quantity: number;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

    @ManyToOne(() => Cart, (cart) => cart.cartItems, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'cartId', referencedColumnName: 'id' }])
    cart: Cart;

    @ManyToOne(() => Product, (product) => product.cartItems, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'productId', referencedColumnName: 'id' }])
    product: Product;

}