import {
    Column,
    CreateDateColumn,
    Entity,
    Index,
    JoinColumn,
    ManyToMany,
    ManyToOne,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';

import { User, OrderItem, Coupon, Address } from '.'

import { OrderStatus } from '../interfaces/interface';

@Index('idx_order_user', ['userId'], {})
@Entity('order', { schema: 'shop' })
export class Order {

    @PrimaryGeneratedColumn({ type: 'bigint', name: 'id' })
    id: string;

    @Column('bigint', { name: 'userId', nullable: true })
    userId: string | null;

    @Column({
        type: "enum",
        enum: OrderStatus,
        default: OrderStatus.Pending
    })
    status: OrderStatus;

    @Column('float', { name: 'subTotal', precision: 12, default: () => "'0'" })
    subTotal: number;

    @Column('float', { name: 'tax', precision: 12, default: () => "'0'" })
    tax: number;

    @Column('float', { name: 'shipping', precision: 12, default: () => "'0'" })
    shipping: number;

    @Column('float', { name: 'total', precision: 12, default: () => "'0'" })
    total: number;

    @Column('float', { name: 'discount', precision: 12, default: () => "'0'" })
    discount: number;

    @Column('text', { name: 'content', nullable: true })
    content: string | null;

    @Column({ type: 'smallint', name: 'addressId', nullable: true })
    addressId: number | null;

    @Column('varchar', { length: 100, nullable: true })
    invoice: string | null;

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

    @ManyToMany(() => Coupon, coupon => coupon.orders)
    coupons?: Coupon[]

    @ManyToOne(() => User, (user) => user.orders, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'userId', referencedColumnName: 'id' }])
    user: User;

    @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
    orderItems: OrderItem[];

    @ManyToOne(() => Address, address => address.orders, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'addressId', referencedColumnName: 'id' }])
    address: Address;

    // @OneToMany(() => Transaction, (transaction) => transaction.order)
    // transactions: Transaction[];

}