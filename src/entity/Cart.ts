import { Entity, PrimaryGeneratedColumn, Column, JoinTable, ManyToMany, OneToMany, JoinColumn, OneToOne } from "typeorm";
import { User, CartItem, Product, Coupon } from '.'
import Address from "./Address";

@Entity()
export class Cart {

    @PrimaryGeneratedColumn()
    id: number;

    @Column('float', { name: 'subTotal', precision: 12, default: () => "'0'" })
    subTotal: number;

    @Column('float', { name: 'tax', precision: 12, default: () => "'0'" })
    tax: number;

    @Column('float', { name: 'shipping', precision: 12, default: () => "'0'" })
    shipping: number;

    @Column('float', { name: 'total', precision: 12, default: () => "'0'" })
    total: number;

    @Column('float', { name: 'discount', precision: 12, default: () => "'0'" })
    discount: number;

    @Column({ type: 'smallint', name: 'addressId', nullable: true })
    addressId: number | null;

    @ManyToMany(() => Coupon, coupon => coupon.carts, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    coupons?: Coupon[]

    @OneToMany(() => CartItem, cartItem => cartItem.cart)
    cartItems?: CartItem[];

    @OneToOne(() => User, user => user.cart)
    user: User;

    @OneToOne(() => Address, address => address.cart, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'addressId', referencedColumnName: 'id' }])
    address?: Address;

}