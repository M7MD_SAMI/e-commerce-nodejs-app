import { Entity, PrimaryGeneratedColumn, Column, JoinTable, ManyToMany, OneToMany, OneToOne, JoinColumn, Index, BeforeInsert, DeleteDateColumn, UpdateDateColumn, CreateDateColumn, AfterInsert, getRepository } from "typeorm";

import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
// import {Contains, IsInt, Length, IsEmail, IsFQDN, IsDate, Min, Max} from "class-validator";
import { Order, OrderItem, CartItem, Cart, Category, Product } from '.'
import Address from "./Address";


export enum UserRole {
    Customer = 1,
    Editor = 2,
    Admin = 3,
    SuperAdmin = 4
}
@Index( 'uq_email', [ 'email' ], { unique: true } )
@Entity()
export class User {

    @PrimaryGeneratedColumn( { type: 'bigint', name: 'id' } )
    id: string;

    @Column('varchar', { nullable: false, length: 50, unique: true })
    email: string;

    @Column('int', { default: UserRole.Customer })
    role: UserRole;

    @Column('varchar', { length: 50, nullable: true })
    phone: string;

    @Column('varchar', { length: 100 })
    userName: string;

    @Column( 'varchar', { name: 'firstName', nullable: true, length: 50 } )
	firstName: string | null;

	@Column( 'varchar', { name: 'middleName', nullable: true, length: 50 } )
	middleName: string | null;

    @Column({ default: null })
    image: string;

    @Column({ select: false })
    password: string;

    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 12);
    }

    async comparePassword(attempt: string): Promise<boolean> {
        return await bcrypt.compare(attempt, this.password);
    }

    token(user: User): string {
        const { id, email } = this;

        return jwt.sign(
            {
                id,
                email,
            },
            process.env.SECRET + '',
            { expiresIn: '90d' },
        );
    }

    @CreateDateColumn({ type: "timestamp" })
    createdAt: Date;

    @UpdateDateColumn({ type: "timestamp" })
    updatedAt: Date;

    @DeleteDateColumn({ name: 'deleted_at', nullable: true })
    deletedAt: Date;

    @Column({ name: 'cartId' })
    cartId: number;

    @OneToMany(() => Product, product => product.user, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    products: Product[];

    @OneToMany(() => Order, order => order.user)
    orders: Order[];

    @OneToMany(() => Address, address => address.user, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    addresses: Address[];

    @OneToOne(() => Cart, cart => cart.user, {
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinColumn([{ name: 'cartId', referencedColumnName: 'id' }])
    cart: Cart;

    @Column({ default: 0 })
    approve: boolean;

}