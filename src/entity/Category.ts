import {Entity, PrimaryGeneratedColumn, Column, JoinTable, ManyToMany} from "typeorm";
import { Product } from '.'

@Entity()
export class Category {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @ManyToMany(type => Product, product => product.categories, {
        // eager: true, onUpdate: 'CASCADE', onDelete: 'CASCADE'
        onUpdate: 'CASCADE', onDelete: 'CASCADE'
    })
    @JoinTable()
    products: Product[]

}