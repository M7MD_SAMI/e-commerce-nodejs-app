# Node.js, Express and Mysql E-Commerce Structure

Building your own RESTful web APIs for e-commerce app built with TypeScript, NodeJS, Express, Mysql, TypeORM.

## How to run

### Running server locally

```
npm install
npm run dev
```

### To Build Project

```
npm run build
```

### Swagger Documentation

http://localhost:4000/api-docs/

Press `CTRL + C` to stop the process.

- [LinkedIN](https://www.linkedin.com/in/m7mdsami/ 'Linkedin') - Mohamed Sami
- contact@m7mdsami.com
